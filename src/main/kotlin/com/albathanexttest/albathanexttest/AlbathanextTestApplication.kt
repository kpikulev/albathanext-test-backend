package com.albathanexttest.albathanexttest

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class AlbathanextTestApplication

fun main(args: Array<String>) {
	runApplication<AlbathanextTestApplication>(*args)
}