package com.albathanexttest.albathanexttest.controller

import com.albathanexttest.albathanexttest.AppProperties
import com.albathanexttest.albathanexttest.model.Movie
import com.albathanexttest.albathanexttest.model.MovieResponse
import com.albathanexttest.albathanexttest.model.MoviesResponse
import com.albathanexttest.albathanexttest.service.MoviesService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("movies")
class MoviesController {
    var logger: Logger = LoggerFactory.getLogger(MoviesController::class.java)

    @Autowired
    private val moviesService: MoviesService? = null

    @Autowired
    lateinit var appProperties: AppProperties

    @GetMapping
    fun getMovies(): MoviesResponse? {
        logger.info("___ GET MOVIES ___");

        val moviesResponse: MoviesResponse? = moviesService?.getMovies()

        if (moviesResponse === null) {
            throw Error("Movies response is empty")
        }

        val movies: Array<Movie>? = moviesResponse.results

        if (!movies.isNullOrEmpty()) {
            for (movie in movies) {
                movie.poster_path = appProperties.imagesBaseUrl + movie.poster_path
            }
        }

        return moviesResponse
    }

    @GetMapping("/{id}")
    fun getMovie(@PathVariable id: String): MovieResponse? {
        logger.info("___ GET MOVIE ___");

        val movie: Movie? = moviesService?.getMovie(id.toInt())

        if (movie === null) {
            throw Error("Movie is empty")
        }

        movie.poster_path = appProperties.imagesBaseUrl + movie.poster_path

        return MovieResponse(movie)
    }
}
