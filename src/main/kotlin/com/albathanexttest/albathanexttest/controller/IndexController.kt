package com.albathanexttest.albathanexttest.controller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class IndexController {
    @GetMapping
    fun featured(): String {
        return "Movies API"
    }
}
