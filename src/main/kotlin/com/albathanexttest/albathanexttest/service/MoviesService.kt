package com.albathanexttest.albathanexttest.service

import com.albathanexttest.albathanexttest.AppProperties
import com.albathanexttest.albathanexttest.model.Movie
import com.albathanexttest.albathanexttest.model.MoviesResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.stereotype.Service

import org.springframework.web.client.RestTemplate


@Service
class MoviesService(restTemplateBuilder: RestTemplateBuilder) {
    private val restTemplate: RestTemplate

    @Autowired
    lateinit var appProperties: AppProperties

    init {
        restTemplate = restTemplateBuilder.build()
    }

    fun getMovies(): MoviesResponse? {
        val url = "https://api.themoviedb.org/3/discover/movie?api_key=" + appProperties.themoviedbApiKey + "&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&with_watch_monetization_types=flatrate"
        return restTemplate.getForObject<MoviesResponse>(url, MoviesResponse::class.java)
    }

    fun getMovie(movieId: Number): Movie? {
        val url = "https://api.themoviedb.org/3/movie/" + movieId + "?api_key=" + appProperties.themoviedbApiKey + "&language=en-US"
        return restTemplate.getForObject<Movie>(url, Movie::class.java)
    }
}