package com.albathanexttest.albathanexttest.model

import java.io.Serializable

class MovieResponse: Serializable {
    constructor(movie: Movie) {
        this.movie = movie
    }

    var movie: Movie? = null
}