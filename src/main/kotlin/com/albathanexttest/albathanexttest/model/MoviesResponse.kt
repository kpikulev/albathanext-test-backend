package com.albathanexttest.albathanexttest.model

import java.io.Serializable

class MoviesResponse: Serializable {
    val page: Number = 0
    val results: Array<Movie>? = null
    val total_pages: Number = 0
    val total_results: Number = 0
}