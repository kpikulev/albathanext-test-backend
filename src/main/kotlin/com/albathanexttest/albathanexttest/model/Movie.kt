package com.albathanexttest.albathanexttest.model

import java.io.Serializable

class Movie : Serializable {
    val id: Number = 0
    val original_title: String? = null
    val tagline: String? = null
    var poster_path: String? = null
    var release_date: String? = null
    var overview: String? = null
    var budget: Number? = null
    var homepage: String? = null
    var revenue: Number? = null
    var vote_average: Number? = null
}