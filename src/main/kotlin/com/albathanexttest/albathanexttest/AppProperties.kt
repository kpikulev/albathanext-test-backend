package com.albathanexttest.albathanexttest

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

@Component
class AppProperties {
    @Value("\${images_base_url}")
    lateinit var imagesBaseUrl: String

    @Value("\${themoviedb_api_key}")
    lateinit var themoviedbApiKey: String
}