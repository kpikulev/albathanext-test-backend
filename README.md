# Albathanext test app

## How to start application:

Start `docker-compose up` from here before running `docker-compose up` from frontend.

### DEV mode

1. Run `docker-compose up backend-dev`
2. After steps above, you can open http://localhost:8080

### PROD mode

1. Run `docker-compose up backend-prod`
2. After steps above, you can open http://localhost:8080
