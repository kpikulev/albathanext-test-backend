gradle bootRun |
while IFS= read -r line; do
  printf "[%s] %s\n" "Running Application Output" "$line";
done 2>&1 &

until curl -f "http://localhost:8080"; do
  echo "Waiting for application to start...";
  sleep 10;
done;

echo "Application started, launch continuous build:"

sh -c "gradle build --continuous \
-x jar \
-x test \
-x classes \
-x testClasses \
-x check \
-x compileTestKotlin" |
while IFS= read -r line; do
  printf "[%s] %s\n" "Build Output" "$line";
done